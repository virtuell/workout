# Beschreibung
Während eines Online-Spiels kommt es häufig vor, dass Du mit Deinen Teamkollegen sprechen musst. Dabei ist es sehr zeitaufwändig ganze Sätze zu schreiben. Aus diesem Grund gibt es einige Abkürzungen, die anstelle von ganzen Sätzen verwendet werden können.

Beispiel

    gg : steht für "Good Game"
    brb: steht für "be right back"

All diese verkürzten Texte können für unerfahrene Spieler verwirrend sein. Sie werden nicht gleich wissen, was

    "gl hf all" (good luck have fun all)

bedeutet.

Du hast die Aufgabe, abgekürzte Sätze in ihre Vollversion umzuwandeln.

## Wortliste

    lol - laugh out loud
    dw - don't worry
    hf - have fun
    gg - good game
    brb - be right back
    g2g - got to go
    wp - well played
    gl - good luck
    imo - in my opinion

# Aufgabe 1

Erzeuge eine Konsolenanwendung die es dem Spieler ermöglicht Abkürzungen einzugeben. Als Ergebnis wird die Bedeutung der Abkürzung ausgegeben.

## Beispielaufruf

```
> ExtendAbbreviations.exe
lol
laugh out loud
```

Das Programm erwartet so lange die Eingabe von Abkürzungen, bis der Spieler mit

    exit

die Anwendung beendet.

# Aufgabe 2

Erweitere das Programm um die Möglichkeit ganze Sätze mit enthaltenen Abkürzungen eingeben zu können.
Enthält ein Wort eine Abkürzung, darf das Programm dieses Wort nicht zerstückeln.

## Beispielaufruf

```
> ExtendAbbreviations.exe 
dw, there is a global variable over there
```

Falsch:
```
don't worry, there is a good luck obal variable over there
```
Richtig:
```
don't worry, there is a global variable over there
```

# Aufgabe 3

Erweitere das Programm um neue Funktionalitäten.

Das Programm startet, ohne dass es die bisherigen Abkürzungen kennt. Stelle dem Benutzer ein Auswahlmenü zur Verfügung, um folgende Funktionen zu verwenden:

- Neue Abkürzung hinzufügen
- Vorhandene Abkürzung bearbeiten
- Vorhandene Abkürzung löschen
- Alle vorhandene Abkürzungen anzeigen
- Satz eingeben und Abkürzungen ersetzen

Beispiel:
```
-------------
- Hauptmenü -
-------------
Funktionen:
(N)eue Abkürzung hinzufügen
(B)earbeite vorhandene Abkürzung
(L)ösche vorhandene Abkürzung
(V)orhandene Abkürzungen anzeigen
(S)atz eingeben und Abkürzungen ersetzen

exit beendet das Programm
-------------------------

Bitte Aktion auswählen: N
Bitte Abkürzung eingeben: gl
Bitte Bedeutung von gl eingeben: good luck
Die Abkürzung gl wurde gespeichert!

Bitte Aktion auswählen: exit
Das Programm wird beendet
```

# Aufgabe 4

Erweitere das Programm um neue Funktionalitäten: 

- Die Anwendung verwendet eine Textdatei als Datenspeicher für Abkürzungen und deren Bedeutungen
- Die Anwendung lädt beim Start alle in der Textdatei vorhandenen Abkürzungen und deren Bedeutungen (sie stehen dem Benutzer also sofort zur Verfügung)
- Bei Beendigung der Anwendung werden alle Änderungen in der Textdatei gespeichert

Verwende das NuGet Paket [System.IO.Abstrations](https://github.com/TestableIO/System.IO.Abstractions) für Dateisystemoperationen (lesen/schreiben), um die Testbarkeit zu verbessern.
