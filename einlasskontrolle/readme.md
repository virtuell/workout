# Beschreibung

Du besitzt ein großes Einkaufszentrum in bester Innenstadtlage. 

In Zeiten von Corona hast Du die Auflage erhalten, Kunden an allen Eingängen über die aktuelle Anzahl der im Einkaufszentrum anwesenden Personen zu informieren.

Hierbei gelten folgende Regeln:

- Die aktuelle Anzahl der anwesenden Personen wird in einer Konsolenanwendung ausgegeben
- Die Anzahl wird durch ein farbiges Ampelfarbensystem dargestellt
  - Grün: Weniger als 50 Personen (Eintritt erlaubt)
  - Gelb: Weniger als 100 Personen (Eintritt erlaubt, Hinweis auf Abstandsregeln einblenden)
  - Rot: Ab 100 Personen (Eintritt verboten)

Um die Aufgabe etwas zu vereinfachen, ist es nicht notwendig, das Sensorsystem zur Erfassung der Personenanzahl zu programmieren.
Du kannst Dich darauf verlassen, dass die Anzahl der anwesenden Personen immer in der Datei ```Data.txt``` im Anwendungsverzeichnis aktualisiert wird.

# Aufgabe 1

Programmiere eine .NET Core Konsolenanwendung, die auf Dateiänderungen in der Datei ```Data.txt``` reagiert. 

Bei einer Dateiänderung muss die Anwendung die Kundschaft über die geänderte Anzahl von Personen im Einkaufszentrum, unter Beachtung der oben aufgeführten Regeln, informieren.

Die Anwendung wird erst durch die Eingabe ```q``` + ```<ENTER>-Taste``` beendet.

# Aufgabe 2

Erweitere die Anwendung um folgende Funktion:

Besonders ältere Kunden haben die Rückmeldung gegeben, dass sie die Zahl der aktuell im Einkaufszentrum anwesenden Personen nur schwer lesen können.

Aus diesem Grund möchtest Du die Anzahl der Personen nun deutlicher hervorheben. Eine Internetrecherche hat das perfekte Verfahren zur Bewältigung dieser Aufgabenstellung erbracht. Verwende:

```
     _    ____   ____ ___ ___        _    ____ _____
    / \  / ___| / ___|_ _|_ _|      / \  |  _ \_   _|
   / _ \ \___ \| |    | | | |_____ / _ \ | |_) || |
  / ___ \ ___) | |___ | | | |_____/ ___ \|  _ < | |
 /_/   \_\____/ \____|___|___|   /_/   \_\_| \_\|_|
```

Die folgende Session ist als Anregung zu verstehen, wie die Ausgabe der anwesenden Personen formatiert werden könnte:

[![asciicast](https://asciinema.org/a/SEKGbmwltxpXrxTScJGRT0Ky5.svg)](https://asciinema.org/a/SEKGbmwltxpXrxTScJGRT0Ky5?autoplay=1&rows=30)

