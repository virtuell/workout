# Beschreibung 

In dieser Kata sollen die "Glücklichen Zahlen" gefunden werden. Diese sind natürliche Zahlen, die mit einem Siebprinzip erzeugt werden. (Ähnlich dem Sieb des Eratosthenes zur Bestimmung von Primzahlen.) 

# Aufgabe 

Finde die glücklichen Zahlen von 1 bis n (n = Eingabe durch den Benutzer) 

Link zu glücklichen Zahlen: https://de.wikipedia.org/wiki/Gl%C3%BCckliche_Zahl


# Beispiel 

```
Eingabe: 30 
```

```
Ausgabe: 1,3,7,9,13,15,21,25 
```
