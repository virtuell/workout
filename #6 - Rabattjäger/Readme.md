# Beschreibung

Der Verkäufer in Deinem Lieblingsgeschäft macht Dir ein Angebot: Du darfst ab sofort bei jedem Preis ab einer Höhe von 10 Euro eine Ziffer Deiner Wahl entfernen. Der resultierende Preis darf auch führende Nullen enthalten die bei der Ausgabe entfernt werden. Die Preisgestaltung in diesem Geschäft ist übrigens außergewöhnlich: Es werden nur Waren zu vollen Eurobeträgen (keine Nachkommastellen) verkauft.

# Aufgabe
Deine Mission: Gib so wenig Geld wie möglich aus!

# Beispielaufrufe:

```
> [Programmname] 3
> 3
```

```
> [Programmname] 21
> 1
```

```
> [Programmname] 132
> 12
```

```
> [Programmname] 104
> 4
```