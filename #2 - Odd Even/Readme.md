# Beschreibung

OddEven ist eine einfache Kata und ähnelt stark der FizzBuzz Übung. 

# Aufgabe

Wenn eine Zahl "Gerade" ist (durch 2 teilbar), soll "Even" ausgegeben werden. 

Wenn eine Zahl "Ungerade" ist (nicht durch 2 teilbar), soll "Odd" ausgegeben werden. 

Wenn eine die Zahl eine Primzahl ist, soll die Zahl ausgegeben werden. 

Es sollen einzelne Zahlen und auch Zahlenbereiche(von-bis) eigegeben werden können. 

Link zu Primzahlen: https://de.wikipedia.org/wiki/Primzahl

# Beispielaufrufe

Beispielaufrufe für die Ausgaben von 1 bis 10: 

```
> OddEven.exe 1 10 
```

oder 

```
> OddEven.exe 10 
```

Führt zu der Konsolenausgabe: 

```
> Eingabe: 1 bis 10  

> Ausgabe: Odd, 2, 3, Even, 5, Even, 7, Even, Odd, Even 
```
