# Beschreibung

Du betreibst einen Copyshop. Beim kassieren hast Du Dich In letzter Zeit häufiger verrechnet, da die Kunden teilweise gleichzeitig mehrere Kopierer verwenden und Du Dir die Anzahl der jeweiligen Kopien nicht merken kannst. Um die Kunden nicht weiter zu verärgern, willst Du nun eine Kassensoftware programmieren und verwenden. 

Du berechnest folgende Preise für Kopien:

    Seite   1 bis  50: 10 Cent/Kopie
    Seite  51 bis 100:  9 Cent/Kopie
    Seite 101 bis 150:  8 Cent/Kopie
    ab Seite 151:       7 Cent/Kopie

**Beispiel:**

Ein Kunde kopiert 99 Seiten. Du berechnest dem Kunden 

*50 * 10 Cent + 49 * 9 Cent*

# Aufgabe 1

Erstelle für diese Aufgabenstellung eine .NET Core Konsolenanwendung.
Die Anwendung besteht aus zwei Klassen: Program und Copyshop

Die Klasse Copyshop implementiert folgende Methoden:
``` csharp
public void Reset(); // Daten zurücksetzen, um neuen Kunden zu bedienen
public void AddCopies(int count); // Kunde kopiert n-Seiten - diese Methode kann mehrfach für einen Kunden aufgerufen werden
public float CalculatePrice(); // Berechnet den Preis anhand der aktuellen Anzahl von Kopien
public int GetCountCopies(); // Gibt die aktuelle Anzahl von Kopien zurück
```
In der Klasse Program dürfen alle Ein-/Ausgaben und Aufrufe der Klasse Copyshop verarbeitet werden.

# Aufgabe 2

Erweitere das Programm um folgende Funktion:

    Als Copyshopbetreiber möchte ich ein Tagesprotokoll über jeden einzelnen Verkauf führen, um die Werte mit dem Geldbetrag in meiner Registrierkasse abgleichen zu können.

Für die Implementation gelten folgende Regeln:

- Die Methode ``` public void Reset()``` der Klasse **Copyshop** erzeugt einen neuen Protokolleintrag in der Tagesprotokolldatei
    - Die Tagesprotokolldateien werden ausgehend vom Ausführungsverzeichns der Anwendung im Unterordner **Protokolle** gespeichert
    - Für jeden Verarbeitungstag wird eine neue Protokolldatei angelegt/verwendet
        - Die Bezeichnung der Protokolldatei folgt dem Muster **Jahr-Monat-Tag.txt** (Beispiel für den 9. Februar 2020: ```Protokolle/2020-02-09.txt```)
- Der Protokolleintrag besteht aus folgenden Informationen
    - Aktueller Zeitstempel  (```DateTime.Now``` im Format STUNDE:MINUTE)
    - Summe (Anzahl) der verkauften Kopien
    - Kalkulierter Preis für die Summe (Anzahl) der verkauften Kopien
- Ein Protokolleintrag wird nur dann erzeugt/gespeichert, wenn die Summe (Anzahl) der verkauften Kopien > 0 ist

**Beispielhafter Auszug aus der Tagesprotokolldatei vom 9. Februar 2020** (```Protokolle/2020-02-09.txt```):

    11:30;5;0.50
    11:35;50;5
    16:44:2;0.2
    16:44;3;0.6
    16:44;6;1.2

**Erläuterung zu den Dateiinhalten:**

Am 9. Februar 2020 hat der Copyshopbetreiber folgende Transaktionen durchgeführt:
- Um 11:30 Uhr 5 Kopien für € 0,50 verkauften
- Um 11:35Uhr 50 Kopien für € 5,- verkauften
- Um 16:44 Uhr 2 Kopien für € 0,20 verkauften
- Um 16:44 Uhr 3 Kopien für € 0,60 verkauften
- Um 16:44 Uhr 6 Kopien für € 1,20 verkauften



# Aufgabe 3

Lagere die Preiskonfiguration in eine Textdatei aus. Sorge dafür, dass in der Preiskonfigurationsdatei beliebig viele Einträge enthalten sein können.

Für die Implementation gelten folgende Regeln:

- Die Preiskonfigurationsdatei wird ausgehend vom Ausführungsverzeichns im Pfad ```Konfiguration/Preiskonfiguration.txt``` gespeichert
- Das Laden der Preiskonfigurationen wird in der Klasse **Copyshop** in der neuen Methode ```public void LoadPrices()``` implementiert
- Stelle dem Benutzer im Hauptmenü die Option zur Verfügung, die Preiskonfiguration erneut zu laden (so kann der Benutzer die Datei Preiskonfiguration.txt manuell ändern und, während die Anwendung aktiv ist, neu laden)

