# Beschreibung 

StringCalculator soll Grundwissen zur Nutzung von Interfaces vermitteln und das Prüfen von Daten vermitteln. Es ist ein Interface "ICalculator" mit der Methode "int Add (string numbers)" zu implementieren. Anschließend soll das Interface implementiert werden. Ziel der Methode ist es, alle Zahlen aus den Parameter "numbers" zu summieren und zurückzuliefern. 

 

# Aufgabe 

Erstellung Interface "ICalculator" mit der Methode "int Add (string numbers)" 

Die Zeichenkette "numbers" kann beliebig viele Zahlen enthalten, die jeweils durch ein Komma separiert werden 

Die Methode soll so stabil wie möglich sein und ungültige Werte ignorieren 

Ungültige oder leere Werte werden als 0 behandelt 

Die Eingabe der Werte soll bei dem Programmaufruf als Parameter erfolgen 
 

# Beispielaufrufe 

```
> Calculator.exe 1,2,3 
> Ausgabe: 6 
```
 
```
> Calculator.exe 
> Ausgabe: 0 
```
 
```
> Calculator.exe 1,,,0  
> Ausgabe: 1 
```
 
```
> Calculator.exe 2,abc,3,vier,5 
> Ausgabe: 10 
```
 

# Erweiterung 

Der Separator soll durch den Benutzer geändert werden können, wird aber nicht explizit angegeben. 

# Beispielaufruf

```
> Calculator.exe 4#6#2#7 
> Ausgabe: 19
```
