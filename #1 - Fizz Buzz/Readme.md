# Beschreibung

Fizz Buzz ist sozusagen das "Hello World" unter den Katas. Es soll auf möglichst einfache Weise zeigen welche Anweisungen und Bestandteile in einem Kata vorkommen. Es soll daher nur der Einführung in Katas bzw. der Programmiersprache dienen. 

# Aufgabe

Wenn eine natürliche Zahl durch 3 teilbar ist soll "fizz" Ausgegeben werden. 

Wenn eine natürliche Zahl durch 5 teilbar ist soll "buzz" Ausgegeben werden. 

Wenn eine natürliche Zahl durch 3 & 5 teilbar ist soll "fizzbuzz" Ausgegeben werden. 

# Beispielaufrufe

```
> FizzBuzz.exe 1 
> Ausgabe: 1 
```

```
> FizzBuzz.exe 2 
> Ausgabe: 2 
```
 
```
> FizzBuzz.exe 3 
> Ausgabe: fizz 
```
 
```
> FizzBuzz.exe 4 
> Ausgabe: 4 
```
 
```
> FizzBuzz.exe 5 
> Ausgabe: buzz 
```

```
> FizzBuzz.exe 6 
> Ausgabe: fizz 
```

[...] 

```
> FizzBuzz.exe 10 
> Ausgabe: buzz 
```

[...] 

```
> FizzBuzz.exe 15 
> Ausgabe: fizzbuzz 
```

[...] 